FROM alpine:20240329 AS builder

ARG THTTPD_VERSION=2.29

# Install all dependencies required for compiling thttpd
RUN apk add gcc musl-dev make wget jq curl npm nodejs rust cargo &&\
# Download thttpd sources
# openssl
  wget https://acme.com/software/thttpd/thttpd-${THTTPD_VERSION}.tar.gz &&\
  tar xzf thttpd-${THTTPD_VERSION}.tar.gz &&\
  mv /thttpd-${THTTPD_VERSION} /thttpd &&\
# Compile thttpd to a static binary which we can copy around
  cd /thttpd &&\
  ./configure &&\
  make CCOPT='-O2 -s -static' thttpd &&\
# Create a non-root user to own the files and run our server
  adduser -D static

#RUN echo http:"//dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
#  apk update && apk upgrade && apk add \
#  gcc musl-dev rust cargo

RUN printf "#!/bin/sh\nbusybox date\n" > /bin/gdate && chmod a+x /bin/gdate &&\
    cargo install htmlq

ENV PATH="/root/.cargo/bin:${PATH}"

WORKDIR /build

COPY package.json .
RUN npm install -g minify mustache marked

COPY src src/
COPY build.sh .

RUN  ./build.sh

# Switch to the scratch image
FROM scratch

EXPOSE 4000

# Copy over the user
COPY --from=builder /etc/passwd /etc/passwd

# Copy the thttpd static binary
COPY --from=builder /thttpd/thttpd /

# Use our non-root user
USER static
WORKDIR /home/static

# Copy the static website
# Use the .dockerignore file to control what ends up inside the image!
COPY --from=builder /build/build ./

# Run thttpd
CMD ["/thttpd", "-D", "-h", "0.0.0.0", "-p", "4000", "-d", "/home/static", "-u", "static", "-l", "-", "-M", "60"]
