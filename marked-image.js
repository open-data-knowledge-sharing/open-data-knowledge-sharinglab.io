#!/usr/bin/env node

const marked = require('marked');
const path = require('path');

const renderer = new marked.Renderer();
renderer.image = function(href, title, text) {
  // const url = href.startsWith('http') ? href : `images/${href}`; // for external links
  // return `<img src="${url}" alt="${text}" title="${title}" />`;

  const imagePath = path.join(__dirname, 'images', href);
  return `<img src="${imagePath}" src="${imagePath}" alt="${text}" title="${title}" />`; // for local references
};

const markdown = `![[20230613_110437.jpg]]`;
const html = marked(markdown, { renderer });

console.log(html);
