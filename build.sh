#!/bin/sh

OUT="build"
SRC="src"
PROJECT="18440910"
STAGE=$OUT/"staging"
BUILD_INFO=$OUT/build.txt

# tear down and re-build the output directory
rm -rf $OUT
mkdir $OUT
mkdir $STAGE
mkdir $OUT/time
mkdir $OUT/style

cp $SRC/assets/stilmallar/style.css $OUT/style/custom.css
minify $OUT/style/custom.css > $OUT/style/custom-min.css

cat $OUT/style/custom-min.css | jq -Rs '{"style":.}' > $OUT/style/style.json
curl -s https://gitlab.com/api/v4/projects/$PROJECT/wikis?with_content=true | jq -cr '.[] | .slug, .' | awk 'NR%2{f=var$0;next} {print >f;close(f)}' var="${STAGE}/"

# Create hash for inline scripts
#cat $OUT/style/custom-min.css | openssl dgst -binary -sha256 | openssl base64 | jq -Rs '{"hash":.}' > $OUT/csshash.json

#CO2
curl -s https://api.websitecarbon.com/b?url=https%3A%2F%2Fnosad.se%2F > $OUT/CO2.json

#Site configuration, brand, logo and links etc
cp $SRC/assets/configuration/site.json $OUT/site.json

FILES="$STAGE/*"
for f in $FILES
do
  echo "Processing $f"
  # take action on each file. $f store current file name
  cat "$f" | jq -cr '.content' | marked -s | jq -Rs '{"content":.}' > "$f"-pre
  jq -s '.[0] * .[1] * .[2] *.[3]' "$f"-pre $OUT/style/style.json $OUT/CO2.json $OUT/site.json | mustache - $SRC/assets/template/index-new.mustache > "$f".html
done

mkdir $OUT/images
find $STAGE/ -iname "*.html" | xargs cat | htmlq --attribute data-large img | sort | uniq | xargs wget --quiet -P $OUT/images/

#find $OUT/images/ -not -name "*.*" -exec mv {} {}.jpg \;


mv $STAGE/*html $OUT
cp -r $SRC/static/site/* $OUT
cp $SRC/static/img/workshop.jpg $OUT/

cd $OUT && ln -s news.html news && cd ..
cd $OUT && ln -s Lista-med-delat-material.html tips && cd ..
cd $OUT && ln -s Digital-Workshopserie.html workshops && cd ..
cd $OUT && ln -s home.html index.html && cd ..

gzip -k $OUT/*html

# Alpine don´t recognize -d
rm -r $STAGE
rm -r $OUT/style
#rm $OUT/style/style.json

echo "Build $(gdate +"%Y-%m-%dT%H:%M:%SZ")" > $BUILD_INFO
